﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class AnimationSpeed : MonoBehaviour {

    public Sprite start;
    public Sprite end;
    private SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
         spriteRenderer = this.GetComponent<SpriteRenderer>();
	}

	public void ToggleSprite() {
        if (spriteRenderer.sprite.name == start.name)
            spriteRenderer.sprite = end;
        else
            spriteRenderer.sprite = start;
	}

    public void ResetSprite()
    {
        spriteRenderer.sprite = start;
    }
}
