﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class FleetManager : MonoBehaviour
{

    public float period = 1;
    public int startingPosition = 16;
    public int PositionDelta = 4; //the amount of pixels that the fleet moves
    private int currentPosition;
    private int rowsDown;
    public const int maxPosition = 42;
    public const int bottomRowLimit = 5;
    public int maxBullets = 3;
    public float minFiringCooldown = 0.01f, maxFiringCooldown = 6f;
    public int bulletSpeed = 150;
    public int maxResets = 20;//the maximum amount of extra difficulty

    public AudioClip[] backgroundClipList;

    public AudioClip[,] toneClips;

    private int fleetCount
    {
        get
        {
            if (fleetList != null)
                return fleetList.Count(x => x.activeInHierarchy == true);
            else
                return 0;
        }
    }

    private GameObject firstInactiveBullet
    {
        get
        {
            return bulletsList.FirstOrDefault(x => x.activeInHierarchy == false);
        }
    }

    public Transform bulletPrefab;

    public PlayerController playerController;

    private List<GameObject> fleetList;
    private List<GameObject> bulletsList;
    private int sign; //either 1 or -1
    private int resets = 0;

    void Start()
    {
        toneClips = new AudioClip[5, 4]; //tempos = 5, tones = 4
        int i, j;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 4; j++)
            {
                toneClips[i, j] = backgroundClipList[i * 4 + j];
            }
        }
        if (playerController == null)
        {
            playerController = FindObjectOfType<PlayerController>();
            Debug.LogWarning("Player not found!");
        }
        if (backgroundClipList == null)
        {
            Debug.LogWarning("Missing Tone Sounds");
        }
        sign = 1;
        currentPosition = startingPosition;
        rowsDown = 0;
        transform.Translate(startingPosition * PositionDelta, 0, 0);

        fleetList = new List<GameObject>();
        bulletsList = new List<GameObject>();

        CreateBullets(maxResets);
        foreach (Transform childTransform in this.transform)
        {
            fleetList.Add(childTransform.gameObject);
            childTransform.tag = this.tag;
            childTransform.GetComponent<Destructible>().audioManager = GetComponent<AudioSource>();
        }
    }

    private float animationTimer = 0; //holds how much time has passed each frame, so that we know when we're above one second
    private float firingTimer = 0;
    private float nextFiring = 0;
    void Update()
    {
        animationTimer += Time.deltaTime;
        firingTimer += Time.deltaTime;

        if (nextFiring == 0)
            SetNewFiringTime();
        if (firingTimer >= nextFiring)
        {
            firingTimer = 0;
            SetNewFiringTime();
            fireRandomTimes(1, maxBullets);
        }

        if (animationTimer >= period)
        {
            animationTimer = 0;
            BroadcastMessage("ToggleSprite", SendMessageOptions.DontRequireReceiver);
            currentPosition++;
            if (currentPosition > maxPosition)
            {
                transform.Translate(0, -PositionDelta * 4, 0);
                currentPosition = 0;
                sign = sign * -1;
                rowsDown++;
                if (rowsDown >= bottomRowLimit)
                {
                    playerController.DisplayGameOver();
                }
                else
                    period = 1 - rowsDown * 0.1f;
            }
            else
                transform.Translate(sign * PositionDelta, 0, 0);
            if (rowsDown < 5)
                Camera.main.GetComponent<AudioSource>().PlayOneShot(GetNewTone());
        }

    }

    void FireOnce()
    {
        if (bulletsList.Count <= 0)
            return;

        GameObject bullet = firstInactiveBullet;
        if (bullet == null)
            return;

        bullet.SetActive(true);
        Transform alienFiring = SelectAciveShip();
        if (alienFiring != null)
        {
            float offset = alienFiring.GetComponent<BoxCollider2D>().bounds.size.x * 0.5f;
            bullet.transform.position = alienFiring.position + new Vector3(offset, 0, 0);
        }
    }

    void CreateBullets(int count)
    {
        if (bulletsList == null)
        {
            Debug.LogError("bullet list not allocated!");
            bulletsList = new List<GameObject>();
        }
        for (int i = 0; i < count; i++)
        {
            var newBullet = (Transform)Instantiate(bulletPrefab, Vector3.zero, Quaternion.identity);
            var newBulletMovement = newBullet.GetComponent<BulletMovement>();
            newBullet.Rotate(new Vector3(0, 0, 180));
            newBulletMovement.speed = bulletSpeed;
            newBullet.tag = this.tag;
            bulletsList.Add(newBullet.gameObject);
            newBullet.gameObject.SetActive(false);
        }
    }
    void ResetFleet()
    {
        foreach (GameObject g in fleetList)
        {
            g.SetActive(true);
        }
        BroadcastMessage("ResetSprite", SendMessageOptions.DontRequireReceiver);
        currentPosition = startingPosition;
        transform.position = new Vector2(-248, 0);
        transform.Translate(startingPosition * PositionDelta, 0, 0);
        rowsDown = 0;
        sign = 1;
        //difficulty increase
        resets = Mathf.Min(resets + 1, maxResets);
        maxBullets = resets;
    }

    public void CheckForEmptyFleet()
    {
        if (fleetCount <= 0)
            ResetFleet();
    }

    Transform SelectAciveShip()
    {
        if (fleetCount != 0)
        {
            int randomEnemy = Random.Range(0, fleetCount - 1);
            return fleetList.Where(x => x.activeInHierarchy == true).ElementAt(randomEnemy).transform;
        }
        else
            return null;
    }

    void SetNewFiringTime()
    {
        nextFiring = Random.Range(minFiringCooldown, maxFiringCooldown);
    }

    void fireRandomTimes(int min, int max)
    {
        if (min < 0)
            min = 0;
        if (max < min)
            max = min;
        int timesToFire = Random.Range(min, max);
        for (int i = 0; i < timesToFire; i++)
            FireOnce();
    }

    int toneIndexer = 0;
    AudioClip GetNewTone()
    {
        if (toneIndexer >= 4)
            toneIndexer = 0;
        AudioClip newClip = toneClips[rowsDown, toneIndexer];
        toneIndexer++;
        return newClip;
    }
}
