﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class PlayerController : MonoBehaviour {

    public Sprite normal, broken, gone;

    public int lives;

    public float movementSpeed;

    public GameObject [] livesDisplay;

    public GameObject gameOverText;

    public Transform bulletPrefab;

    private GameObject playerBullet;

    private SpriteRenderer spriteRenderer;

    private AnimationData destructionAnimation;

    private bool canFire = true;

	void Start () {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
        //if(boomSound == null || firingSound==null || AudioManager == null)
          //  Debug.LogError("Sound effects for player are missing!");
        if (livesDisplay == null)
            Debug.LogError("Lives Display is empty");
        if (gameOverText == null)
            Debug.LogError("GameOver missing!");
        if (normal ==null || broken ==null || gone ==null)
            Debug.LogError("Sprites missing!");
        if (bulletPrefab == null)
            Debug.LogError("Player Bullet missing!");
        playerBullet = ((Transform)Instantiate(bulletPrefab, Vector2.zero, Quaternion.identity)).gameObject;
        playerBullet.GetComponent<BulletMovement>().speed = 300;
        playerBullet.tag = this.tag;
        playerBullet.SetActive(false);

        destructionAnimation = new AnimationData(new Sprite[] { normal, broken, gone }, 0.5f, null, spriteRenderer);
	}

    void Update()
    {
        transform.Translate(Input.GetAxis("Horizontal") * movementSpeed *Time.deltaTime, 0, 0);
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -248, 220), transform.position.y, transform.position.z);
        if (Input.GetKey(KeyCode.Space))
            AttemptToFire();
        if (Input.GetKeyDown(KeyCode.P))
            TogglePause();
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (!coll.CompareTag(this.tag))
            PlayDeathAnimation();
    }

    bool isPaused = false;
    void TogglePause()
    {
        if (isPaused)
        {
            Time.timeScale = 1;
            isPaused = false;
            AudioListener.pause = false;
        }
        else
        {
            Time.timeScale = 0;
            isPaused = true;
            AudioListener.pause = true;
        }
    }

    void AttemptToFire()
    {
        if (!playerBullet.activeInHierarchy)
            Fire();
    }

    void Fire()
    {
        if (canFire == false)
            return;
        playerBullet.SetActive(true);
        playerBullet.transform.position = new Vector3(transform.position.x + 10, transform.position.y + 12, 0);
    }

    void PlayDeathAnimation()
    {
        GetComponent<Collider2D>().enabled = false; //you can't take damage after you die
        canFire = false;
        StartCoroutine(DeathAnimation());

    }

    void AttemptRespawn()
    {
        if (lives > 0)
            Respawn();
        else
            DisplayGameOver();
    }

    void Respawn()
    {
        GetComponent<Collider2D>().enabled = true; //you can take damage again
        canFire = true;
        spriteRenderer.sprite = normal;
    }

    public void DisplayGameOver()
    {
        AudioListener.pause = true;
        Time.timeScale = 0;
        gameOverText.SetActive(true);
    }

	IEnumerator DeathAnimation () {
        GetComponent<AudioSource>().Play();
        spriteRenderer.sprite = broken;
        yield return new WaitForSeconds(0.5f);
        spriteRenderer.sprite = gone;
        yield return new WaitForSeconds(0.5f);
        lives -= 1;
        livesDisplay[lives].SetActive(false); //we're at 2 lives, so to disable the 3rd life, we just disable lives[2] since arrays are 0 indexed
        AttemptRespawn();
	}
}
