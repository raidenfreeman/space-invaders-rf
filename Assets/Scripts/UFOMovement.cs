﻿using UnityEngine;
using System.Collections;

public class UFOMovement : MonoBehaviour
{

    public int leftBoundary = -315, rightBoundary = 267;//usually -315, 267;

    public GameObject bonusUFO;

    public float maxAppearanceDelay = 20f, minAppearanceDelay = 0.5f;

    public float minAppearanceTime = 10f;

    public float speed = 58;

    private bool isMoving;

    void Start()
    {

    }

    private float timer = 0;
    private float nextAppearanceTime = 0;
    private float sign = 1f;
    void Update()
    {
        timer += Time.deltaTime;
        
        if(isMoving == true)
        {
            bonusUFO.transform.Translate(sign * speed * Time.deltaTime, 0, 0);
            if (bonusUFO.activeInHierarchy == false || bonusUFO.transform.position.x > rightBoundary || bonusUFO.transform.position.x < leftBoundary)
            {
                Disappear();
            }
        }
        else
        {
            if (timer >= minAppearanceTime)
            {
                if (nextAppearanceTime == 0)
                {
                    SetNextAppearanceTime();
                }
                if (timer >= nextAppearanceTime)
                {
                    Respawn();
                }
            }
        }
    }

    void Respawn()
    {
        isMoving = true;
        bonusUFO.SetActive(true);
        sign = Mathf.Sign(Random.Range(-1f, 1f));
        int newPosition = sign > 0 ? leftBoundary +1 : rightBoundary-1;
        bonusUFO.transform.position = new Vector2(newPosition, transform.position.y);
    }

    void Disappear()
    {
        nextAppearanceTime = 0;
        isMoving = false;
        bonusUFO.SetActive(false);
    }

    void SetNextAppearanceTime()
    {
        nextAppearanceTime = timer + Random.Range(minAppearanceDelay, maxAppearanceDelay);
    }
}
